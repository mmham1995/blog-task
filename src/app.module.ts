import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigurationModule } from './modules/config/config.module';
import { ConfigurationService } from './modules/config/config.service';
import { ArticleModule } from './modules/Article/article.module';
import { AuthorModule } from './modules/Author/author.module';

@Module({
  imports: [
    ConfigurationModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigurationModule],
      useFactory: (configService: ConfigurationService) => ({
        type: 'mysql',
        host: configService.databaseHost,
        port: configService.databasePort,
        username: configService.databaseUsername,
        password: configService.databasePassword,
        database: configService.databaseName,
        supportBigNumbers: true,
        entities: ['dist/**/*.entity{.ts,.js}'],
        synchronize: true, // remove in production
        autoLoadEntities: true,
        logging: false,
        logger: 'advanced-console',
      }),
      inject: [ConfigurationService],
    }),
    AuthorModule,
    ArticleModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

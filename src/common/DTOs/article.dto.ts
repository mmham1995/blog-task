import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class SaveArticleDTO {
  @ApiProperty({ description: 'title', required: true })
  @IsString()
  title: string;

  @ApiProperty({ description: 'body', required: true })
  @IsString()
  body: string;

  @ApiProperty()
  @IsNumber()
  authorId: number; // id of author
}

export class AddCommentDTO {
  @ApiProperty()
  @IsNumber()
  articleId: number; // article id

  @ApiProperty()
  @IsNumber()
  authorId: number; // author id

  @ApiProperty()
  @IsString()
  commentText: string;
}

export class ElasticSearchBody {
  size: number;
  from: number;
  query: any;

  constructor(size: number, from: number, query: any) {
    this.size = size;
    this.from = from;
    this.query = query;
  }
}

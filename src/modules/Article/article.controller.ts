import {
  Controller,
  UseGuards,
  Post,
  Body,
  Get,
  Param,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AddCommentDTO, SaveArticleDTO } from './../../common/DTOs/article.dto';
import { AuthEndPointGuard } from './../../common/guards/authEndpoints.guard';
import { Article } from './../../entities/article.entity';
import { ArticleService } from './article.service';

@ApiTags('Article')
@Controller('Article')
export class ArticleController {
  /* CRUD End Points for Article Created By Override */

  constructor(private service: ArticleService) {}
  /* POST Article End Point */
  @UseGuards(AuthEndPointGuard)
  @Post()
  async saveArticle(@Body() req: SaveArticleDTO): Promise<Article> {
    return this.service.save(req);
  }

  /* GET All Articles End Point */
  @UseGuards(AuthEndPointGuard)
  @Get('/all')
  getAllArticles(): Promise<Article[]> {
    console.log('er');
    return this.service.findAll();
  }

  /* GET One Article End Point */
  @UseGuards(AuthEndPointGuard)
  @Get(':id')
  findOne(@Param('id') id: number): Promise<Article> {
    return this.service.findOne(id);
  }

  /* GET One Article End Point */
  @UseGuards(AuthEndPointGuard)
  @Post('/addComment')
  addComment(@Body() req: AddCommentDTO): Promise<Article> {
    return this.service.addComments(req);
  }

  @UseGuards(AuthEndPointGuard)
  @Put('/:id/addorRemoveLike/:authorId')
  async addorRemoveLike(
    @Param('id') id: number,
    @Param('authorId') authorId: number,
  ) {
    return await this.service.likeOrUnLikeArticle(authorId, id);
  }

  @Get('/query/search')
  async search(@Query('word') word: string): Promise<Article[]> {
    return await this.service.search(word);
  }

  @Get('/sort/likes')
  async sortByLikes(): Promise<Article[]> {
    return await this.service.sortByNumberLikes();
  }
  /* End of Article Controller Class 
 
 */
}

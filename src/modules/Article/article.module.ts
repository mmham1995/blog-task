import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Article } from './../../entities/article.entity';
import { Author } from './../../entities/author.entity';
import { Comment } from './../../entities/comment.entity';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.service';
@Module({
  imports: [TypeOrmModule.forFeature([Author, Article, Comment])],
  controllers: [ArticleController],
  providers: [ArticleService],
  exports: [ArticleService],
})
export class ArticleModule {}

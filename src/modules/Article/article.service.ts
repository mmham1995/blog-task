import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AddCommentDTO, SaveArticleDTO } from './../../common/DTOs/article.dto';
import { ErrorMessages } from './../../common/enums/errorMessages.enum';
import { Article } from './../../entities/article.entity';
import { Author } from './../../entities/author.entity';
import { Comment } from './../../entities/comment.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ArticleService {
  // CRUD Entity for Article Created By Override
  constructor(
    @InjectRepository(Article) private readonly repo: Repository<Article>,
    @InjectRepository(Author) private readonly authorRepo: Repository<Author>,
    @InjectRepository(Comment)
    private readonly commentRepo: Repository<Comment>,
  ) {}

  // findAll Articles Created By Override
  async findAll(): Promise<Article[]> {
    const queryBuilder = this.repo.createQueryBuilder('article');
    queryBuilder.leftJoinAndSelect('article.author', 'author');
    queryBuilder.leftJoinAndMapMany(
      'article.comments',
      Comment,
      'comment',
      'comment.article.id=article.id',
    );

    return await queryBuilder.getMany();
  }

  // findOne Article Created By Override
  async findOne(id: number): Promise<Article> {
    const queryBuilder = this.repo.createQueryBuilder('article');
    queryBuilder.leftJoinAndSelect('article.author', 'author');
    queryBuilder.leftJoinAndMapMany(
      'article.comments',
      Comment,
      'comment',
      'comment.article.id=article.id',
    );
    queryBuilder.where('article.id = :id', { id });

    const found = await queryBuilder.getOne();
    if (!found) {
      throw new NotFoundException(ErrorMessages.invalidCredentials);
    }

    return found;
  }

  // save Article Created By Override
  async save(req: SaveArticleDTO): Promise<Article> {
    const author = await this.authorRepo.findOne({
      where: { id: req.authorId },
    });

    if (!author) {
      throw new NotFoundException(ErrorMessages.invalidCredentials);
    }

    const newArticle = new Article();
    newArticle.title = req.title;
    newArticle.body = req.body;
    newArticle.author = author;
    return await this.repo.save(newArticle);
  }

  async addComments(req: AddCommentDTO): Promise<Article> {
    const newComment = new Comment();
    newComment.commentDate = new Date();
    newComment.commentText = req.commentText;

    const author = await this.authorRepo.findOne({
      where: { id: req.authorId },
    });

    if (!author) {
      throw new NotFoundException(ErrorMessages.invalidCredentials);
    }

    newComment.author = author;

    const queryBuilder = this.repo.createQueryBuilder('article');
    queryBuilder.leftJoinAndSelect('article.author', 'author');
    queryBuilder.leftJoinAndMapMany(
      'article.comments',
      Comment,
      'comment',
      'comment.article.id=article.id',
    );
    queryBuilder.where('article.id = :id', { id: req.articleId });

    const article = await queryBuilder.getOne();

    if (!article) {
      throw new NotFoundException(ErrorMessages.invalidCredentials);
    }

    newComment.article = article;

    await this.commentRepo.save(newComment);

    return await queryBuilder.getOne();
  }

  async likeOrUnLikeArticle(authorId: number, articleId: number) {
    const queryBuilder = this.authorRepo.createQueryBuilder('author');
    queryBuilder.leftJoinAndSelect('author.likes', 'likes');
    queryBuilder.where('author.id = :id', { id: authorId });
    const author = await queryBuilder.getOne();
    if (!author) {
      throw new NotFoundException(ErrorMessages.invalidCredentials);
    }
    const article = await this.repo.findOne({ id: articleId });
    if (!article) {
      throw new NotFoundException(ErrorMessages.invalidCredentials);
    }
    let likes: Article[] = [];
    let isLiked = true;
    if (author.likes.length > 0) {
      const likedBefore = author.likes.filter((like) => like.id == article.id);
      if (likedBefore.length > 0) {
        isLiked = false;
        likes = author.likes.filter((like) => like.id != article.id);
      } else {
        likes = author.likes;
        likes.push(article);
      }
    } else {
      likes.push(article);
    }
    author.likes = likes;
    await this.authorRepo.save(author);

    return { isLiked };
  }

  async search(query: string): Promise<Article[]> {
    const queryBuilder = this.repo.createQueryBuilder('article');
    queryBuilder.leftJoinAndSelect('article.author', 'author');
    queryBuilder.leftJoinAndMapMany(
      'article.comments',
      Comment,
      'comment',
      'comment.article.id=article.id',
    );
    queryBuilder.where(`article.title LIKE :title`, {
      title: '%' + query + '%',
    });

    return await queryBuilder.getMany();
  }

  async sortByNumberLikes(): Promise<Article[]> {
    const queryBuilder = this.repo.createQueryBuilder('article');
    queryBuilder.leftJoinAndSelect('article.author', 'author');
    queryBuilder.leftJoinAndMapMany(
      'article.comments',
      Comment,
      'comment',
      'comment.article.id=article.id',
    );

    const articles = await queryBuilder.getMany();
    if (articles.length > 0) {
      for await (const article of articles) {
        // eslint-disable-next-line prefer-const
        let qb = this.authorRepo.createQueryBuilder('author');
        qb.leftJoinAndSelect('author.likes', 'likes');
        qb.where('author.id = :id', { id: article.author.id });
        const author = await qb.getOne();
        article.numberOfLikes = author.likes.length;
      }

      // ascending order
      articles.sort((a, b) => a.numberOfLikes - b.numberOfLikes);
    }

    return articles.map((article) => {
      article.numberOfLikes = undefined;
      return article;
    });
  }
  // end of Article sevice
}

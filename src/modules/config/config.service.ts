import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
/**
 * Service dealing with app config based operations.
 *
 * @class
 */
@Injectable()
export class ConfigurationService {
  constructor(private configService: ConfigService) {}

  get port(): number {
    return Number(this.configService.get<number>('app.port'));
  }
  get databaseHost(): string {
    return this.configService.get<string>('app.databaseHost');
  }
  get databasePort(): number {
    return Number(this.configService.get<number>('app.databasePort'));
  }
  get databaseUsername(): string {
    return this.configService.get<string>('app.databaseUsername');
  }
  get databasePassword(): string {
    return this.configService.get<string>('app.databasePassword');
  }
  get databaseName(): string {
    return this.configService.get<string>('app.databaseName');
  }
}

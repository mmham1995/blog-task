import { Controller, UseGuards, Post, Body, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthEndPointGuard } from './../../common/guards/authEndpoints.guard';
import { Author } from './../../entities/author.entity';
import { AuthorService } from './author.service';

@ApiTags('Author')
@Controller('Author')
export class AuthorController {
  /* CRUD End Points for Author Created By Override */

  constructor(private service: AuthorService) {}
  /* POST Author End Point */
  @UseGuards(AuthEndPointGuard)
  @Post()
  async saveAuthor(@Body() req: Author): Promise<Author> {
    return this.service.save(req);
  }

  /* GET All Authors End Point */
  @UseGuards(AuthEndPointGuard)
  @Get('/all')
  getAllAuthors(): Promise<Author[]> {
    return this.service.findAll();
  }

  /* GET One Author End Point */
  @UseGuards(AuthEndPointGuard)
  @Get(':id')
  findOne(@Param('id') id: string): Promise<Author> {
    return this.service.findOne(id);
  }

  /* End of Author Controller Class 
 
 */
}

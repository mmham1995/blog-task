import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ErrorMessages } from './../../common/enums/errorMessages.enum';
import { Author } from './../../entities/author.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AuthorService {
  // CRUD Entity for Author Created By Override
  constructor(
    @InjectRepository(Author) private readonly repo: Repository<Author>,
  ) {}

  // save Author Created By Override
  async save(req: Author): Promise<Author> {
    return await this.repo.save(req);
  }

  // findAll Authors Created By Override
  async findAll(): Promise<Author[]> {
    return await this.repo.find();
  }

  // findOne Author Created By Override
  async findOne(id: string): Promise<Author> {
    const author = await this.repo.findOne(id);
    if (!author) {
      throw new NotFoundException(ErrorMessages.invalidCredentials);
    }

    return author;
  }

  // end of Author sevice
}

import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ConfigurationService } from './modules/config/config.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // Get app config for cors settings and starting the app.
  const config: ConfigurationService = app.get('ConfigurationService');

  const options = new DocumentBuilder()
    .setTitle('Blog Services')
    .setDescription('The API description')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  app.useGlobalPipes(new ValidationPipe());
  await app.listen(config.port);
}
bootstrap();

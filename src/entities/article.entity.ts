import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { BasicEntity } from './../shared/basic.entity';
import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { Author } from './author.entity';
import { Comment } from './comment.entity';

@Entity()
export class Article extends BasicEntity {
  @ApiProperty({ description: 'title', required: true })
  @Column()
  @IsString()
  title: string;

  @ApiProperty({ description: 'body', required: true })
  @Column({ type: 'text' })
  @IsString()
  body: string;

  @ApiProperty({ type: () => Author })
  @ManyToOne(() => Author)
  author: Author;

  @ApiProperty({ type: () => Comment, isArray: true, readOnly: true })
  @OneToMany(() => Comment, (comment) => comment.article)
  comments: Comment[];

  @ApiProperty({ readOnly: true })
  numberOfLikes: number;
}

import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { BasicEntity } from './../shared/basic.entity';
import { Column, Entity, JoinTable, ManyToMany } from 'typeorm';
import { Article } from './article.entity';

@Entity()
export class Author extends BasicEntity {
  @ApiProperty({ description: 'author full name', required: true })
  @Column()
  @IsString()
  name: string;

  @ApiProperty({ description: 'job Title', required: true })
  @Column()
  @IsString()
  jobTitle: string;

  @ManyToMany(() => Article, { cascade: true })
  @JoinTable()
  likes?: Article[];
}

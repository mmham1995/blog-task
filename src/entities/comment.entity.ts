import { ApiProperty } from '@nestjs/swagger';
import { BasicEntity } from './../shared/basic.entity';
import { Entity, Column, ManyToOne } from 'typeorm';
import { Article } from './article.entity';
import { Author } from './author.entity';

@Entity()
export class Comment extends BasicEntity {
  @ApiProperty({ type: () => Article })
  @ManyToOne(() => Article, (article) => article.comments)
  article: Article;

  @ApiProperty({ type: () => Author })
  @ManyToOne(() => Author)
  author: Author;

  @ApiProperty()
  @Column({ type: 'text' })
  commentText: string;

  @ApiProperty()
  @Column({ type: 'datetime' })
  commentDate: Date;
}

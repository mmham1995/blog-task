- The Apis Base URL: http://172.105.90.77:4000/

- Api Swagger Documentation: http://172.105.90.77:4000/api/

- To Run APis In Postman:
   - Go to 'authorization' tab 
   - Choose 'Bearer Token'
   - Put value of Vqwertyuiopasdfghjklzxcvbnm123456

- To Create Environment File:
   - Make a file called '.env'
   - Copy 'ENV-FILE-SAMPLE' and paste in .env file and change the credentials

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
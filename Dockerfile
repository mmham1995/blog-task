FROM node:11-alpine 

RUN mkdir -p /blog

WORKDIR /blog

COPY package.json /blog

RUN npm install

COPY . /blog

RUN npm run build 

CMD [ "npm", "run", "start:prod" ]